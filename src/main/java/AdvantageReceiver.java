class AdvantageReceiver implements ResultProvider {

    private final MyTennisGame game;
    private final ResultProvider nextResult;

    public AdvantageReceiver(MyTennisGame game, ResultProvider nextResult) {
        this.game = game;
        this.nextResult = nextResult;
    }

    @Override
    public TennisResult getResult() {
        if (game.receiverHasAdvantage())
            return new TennisResult("Advantage " + game.receiver, "");
        return this.nextResult.getResult();
    }
}
