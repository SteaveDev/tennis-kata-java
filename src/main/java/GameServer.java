class GameServer implements ResultProvider {
    private final MyTennisGame game;
    private final ResultProvider nextResult;

    public GameServer(MyTennisGame game, ResultProvider nextResult) {
        this.game = game;
        this.nextResult = nextResult;
    }

    @Override
    public TennisResult getResult() {
        if (game.serverHasWon())
            return new TennisResult("Win for " + game.server, "");
        return this.nextResult.getResult();
    }
}
