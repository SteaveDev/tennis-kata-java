class GameReceiver implements ResultProvider {
    private final MyTennisGame game;
    private final ResultProvider nextResult;

    public GameReceiver(MyTennisGame game, ResultProvider nextResult) {
        this.game = game;
        this.nextResult = nextResult;
    }

    @Override
    public TennisResult getResult() {
        if (game.receiverHasWon())
            return new TennisResult("Win for " + game.receiver, "");
        return this.nextResult.getResult();
    }
}
