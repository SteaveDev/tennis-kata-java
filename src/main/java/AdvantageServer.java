class AdvantageServer implements ResultProvider {
    private final MyTennisGame game;
    private final ResultProvider nextResult;

    public AdvantageServer(MyTennisGame game, ResultProvider nextResult) {
        this.game = game;
        this.nextResult = nextResult;
    }

    @Override
    public TennisResult getResult() {
        if (game.serverHasAdvantage())
            return new TennisResult("Advantage " + game.server, "");
        return this.nextResult.getResult();
    }
}
