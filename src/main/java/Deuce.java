class Deuce implements ResultProvider {
    private final MyTennisGame game;
    private final ResultProvider nextResult;

    public Deuce(MyTennisGame game, ResultProvider nextResult) {
        this.game = game;
        this.nextResult = nextResult;
    }

    @Override
    public TennisResult getResult() {
        if (game.isDeuce())
            return new TennisResult("Deuce", "");
        return this.nextResult.getResult();
    }
}
